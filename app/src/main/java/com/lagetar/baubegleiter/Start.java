package com.lagetar.baubegleiter;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.lagetar.baubegleiter.domain.Event;
import com.lagetar.baubegleiter.fragment.CalenderFragment;
import com.lagetar.baubegleiter.fragment.ProjectsFragment;
import com.lagetar.baubegleiter.fragment.SettingsFragment;
import com.lagetar.baubegleiter.fragment.SynchronizationFragment;
import com.lagetar.baubegleiter.fragment.TimeKeepFragment;
import com.lagetar.baubegleiter.webrequest.GetProjectImage;
import com.lagetar.baubegleiter.webrequest.GetProjectImagesList;
import com.lagetar.baubegleiter.webrequest.GetProjects;
import com.lagetar.baubegleiter.webrequest.GetTimekeepings;
import com.lagetar.baubegleiter.webrequest.GetToken;
import com.lagetar.baubegleiter.webrequest.PostProjectImage;
import com.lagetar.baubegleiter.webrequest.PostTimeKeepData;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Start extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final int PICK_IMAGE = 1;
    public static final int SHOW_FULLCREEN_PICTURE = 3;
    private static final int TAKE_PICTURE = 2;
    private String token = null;
    private String tokenExpires = null;
    private String lastProjectsSyncTime = null;
    private String lastTimeKeepingsSyncTime = null;
    private String imageFilePath;
    private String imageFileName;
    SQLiteDatabase database = null;
    private boolean getProjectsCalledOnStartup = false;
    private int currentId = 0;
    private Event event = null;

    private String user = null;
    private String password = null;
    private String currentFragment = "";

    private SettingsFragment fragmentSettings = null;
    private ProjectsFragment fragmentProjects = null;
    private CalenderFragment fragmentCalender = null;
    private TimeKeepFragment fragmentTimeKeep = null;
    private SynchronizationFragment fragmentSynchronization = null;

    public interface FinishGetUserPasswordAndGotToken {
        void onFinished();
    }

    private boolean isDateTimeValid() {
        try {
            if (tokenExpires == null) {
                return false;
            }

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date tokenTime = formatter.parse(tokenExpires);
            Date currentDate = new Date();
            formatter.format(currentDate);

            return currentDate.compareTo(tokenTime) < 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Log.v("Permission Error","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        } else {
            //permission is automatically granted on sdk<23 upon installation
            Log.v("Permission","Permission is granted");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* TODO EMAIL BUTTON IS HERE
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Hier wird dann Email verschickt an alle", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        fragmentCalender = new CalenderFragment();
        fragmentTimeKeep = new TimeKeepFragment();
        fragmentProjects = new ProjectsFragment();
        fragmentSynchronization = new SynchronizationFragment();
        checkPermission();

        this.initializeDatabase();
        this.getUserPassword(new FinishGetUserPasswordAndGotToken() {
            @Override
            public void onFinished() {
                fragmentSettings = new SettingsFragment();
                Bundle settingsBundle = new Bundle();
                settingsBundle.putString("user", user);
                settingsBundle.putString("password", password);
                fragmentSettings.setArguments(settingsBundle);
                /*ConstraintLayout layout = (ConstraintLayout) findViewById(R.id.content_start);
                layout.removeAllViewsInLayout();*/
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                if (user.equals("") || password.equals("") || user.isEmpty() || password.isEmpty()) {
                    fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id.content_start, fragmentSettings);
                    currentId = R.id.nav_settings;
                } else {
                    getTimekeepings();
                    getProjects();
                    fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id.content_start, fragmentCalender);
                    getProjectsCalledOnStartup = true;
                    currentId = R.id.nav_calender;

                }

                fragmentTransaction.commitAllowingStateLoss();
                fragmentManager.executePendingTransactions();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    boolean doesTableExist(SQLiteDatabase db, String tableName) {
        if (tableName == null || db == null || !db.isOpen()) {
            return false;
        }

        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?", new String[] {"table", tableName});
        if (!cursor.moveToFirst()) {
            return false;
        }

        int count = cursor.getInt(0);
        cursor.close();
        return count > 0;
    }

    private void getUserPassword(FinishGetUserPasswordAndGotToken finishGetUserPasswordAndGotToken) {
        try {
            database = BaubegleiterApplication.getInstance().getDatabase();
            Cursor results = database.rawQuery("Select * from login", null);
            if (results != null && results.moveToFirst()) {
                user = results.getString(results.getColumnIndex("user"));
                password = results.getString(results.getColumnIndex("password"));
                results.close();
                if (!user.isEmpty() && !password.isEmpty() && !user.equals(" ") && !password.equals(" ")) {
                    this.getToken(user, password, false, finishGetUserPasswordAndGotToken);
                } else {
                    finishGetUserPasswordAndGotToken.onFinished();
                }
            }
        } catch (Exception e) {
            Log.d("User Password fails", e.getMessage());
            finishGetUserPasswordAndGotToken.onFinished();
        }
    }

    private void initializeDatabase() {
        try {
            database = BaubegleiterApplication.getInstance().getDatabase();
            String version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            BaubegleiterApplication.getInstance().setVersion(version);
            if (this.doesTableExist(database, "version") == false) {
                database.execSQL("CREATE TABLE IF NOT EXISTS version(version String);");
                database.execSQL("INSERT INTO version VALUES('" + version + "');");
                database.execSQL("CREATE TABLE IF NOT EXISTS login(user String, password String);");
                database.execSQL("INSERT INTO login VALUES(' ',' ');");
                clearApplicationData(version);
                lastProjectsSyncTime = "2018-09-01 00:00:00";
                lastTimeKeepingsSyncTime = "2018-09-01 00:00:00";
            } else {
                /* UPGRADE path comes here from version to version*/

                Cursor resultSet = database.rawQuery("Select version from version", null);
                double currentVersion = 0;
                if (resultSet != null) {
                    resultSet.moveToFirst();
                    String checkVersion = resultSet.getString(resultSet.getColumnIndex("version"));
                    int counter = 0;
                    int position = 0;
                    for (int i = 0; i < checkVersion.length(); i++) {
                        if (checkVersion.charAt(i) == '.') {
                            counter++;
                            position = i;
                        }
                    }

                    if (counter > 1) {
                        checkVersion = checkVersion.substring(0, position);
                    }

                    currentVersion = Double.parseDouble(checkVersion);

                    if (currentVersion < 1.0) {
                        //TODO Change database settings here as upgrade path
                        //database.execSQL("UPDATE settings SET BackgroundMode='0'");
                    }

                    resultSet.close();
                }

                Cursor resultSetProjectSync = database.rawQuery("Select timestamp from synchronization WHERE name='projects'", null);
                if (resultSetProjectSync != null) {
                    resultSetProjectSync.moveToFirst();
                    lastProjectsSyncTime = resultSetProjectSync.getString(resultSetProjectSync.getColumnIndex("timestamp"));
                    resultSetProjectSync.close();
                } else {
                    lastProjectsSyncTime = "2018-09-01 00:00:00";
                }

                Cursor resultTimeKeepingsSync = database.rawQuery("Select timestamp from synchronization WHERE name='timekeepings'", null);
                if (resultTimeKeepingsSync != null) {
                    resultTimeKeepingsSync.moveToFirst();
                    lastTimeKeepingsSyncTime = resultTimeKeepingsSync.getString(resultTimeKeepingsSync.getColumnIndex("timestamp"));
                    resultTimeKeepingsSync.close();
                } else {
                    lastTimeKeepingsSyncTime = "2018-09-01 00:00:00";
                }
            }
        } catch (Exception e) {
            Log.d("Init database fails", e.getMessage());
        }
    }

    public void clearApplicationData(String version) {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()){
            String[] children = appDir.list();
            for (String s : children){
                if (!s.equals("lib") && !s.equals("databases")){
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "File /data/data/APP_PACKAGE/" + s +" DELETED");
                } else if (s.equals("databases")) {
                    File directory = new File(appDir, s);
                    for (File file: directory.listFiles()) {
                        String databaseName = file.getName();
                        if (!databaseName.equals(DatabaseHelper.DB_NAME)) {
                            file.delete();
                        }
                    }
                }
            }
        }
    }

    public void setEventToChange(Event event) {
        this.event = event;
    }

    public void changeFragment(String fragment) {
        if (fragment.equals(getString(R.string.menu_timekeep))) {
            fragmentTimeKeep.setChangeEvent(this.event);
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id.content_start, fragmentTimeKeep);
            fragmentTransaction.commitAllowingStateLoss();
            fragmentManager.executePendingTransactions();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (currentFragment.equals("timekeep")) {
            if (fragmentTimeKeep.onBackPressed()) {
                super.onBackPressed();
            } else {
                return;
            }
        }

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void startTimeKeepFragment(CalendarDay day, int time) {
        String date = null;
        if (day != null) {
            date = day.getYear() + "-" + day.getMonth() + "-" + day.getDay();
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            date = sdf.format(c.getTime());
        }

        fragmentTimeKeep.setDateAndTimeDataFromCalendar(date, time);
        currentFragment = "timekeep";
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id.content_start, fragmentTimeKeep);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public static void showProjectImages(int projectId) {
        //Switch to created fragment of pictures here
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        boolean updateCurrentFragment = true;
        if (currentId == 0) {
            currentId = id;
        } else {
            if (currentId == id) {
                updateCurrentFragment = false;
            } else {
                currentId = id;
            }
        }

        if (updateCurrentFragment) {
            // Handle the camera action
            //TODO this is how to open camer openCameraIntent();
            /*ConstraintLayout layout = (ConstraintLayout) findViewById(R.id.content_start);
            layout.removeAllViewsInLayout();*/
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            if (id == R.id.nav_calender) {
                currentFragment = "calendar";
                fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id.content_start, fragmentCalender);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();
            } else if (id == R.id.nav_show_projects) {
                currentFragment = "projects";
                fragmentProjects.setMode(ProjectsFragment.PROJECTS_MODE_VIEW);
                fragmentProjects.setToken(token);
                fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id.content_start, fragmentProjects);
                fragmentTransaction.commit();
                fragmentManager.executePendingTransactions();
            } else if (id == R.id.nav_timekeep) {
                currentFragment = "timekeep";
                fragmentTimeKeep.setToken(token);
                fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id.content_start, fragmentTimeKeep);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();
            } else if (id == R.id.nav_show_syncrhonization) {
                currentFragment = "synchronization";
                fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id.content_start, fragmentSynchronization);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();
            } else if (id == R.id.nav_settings) {
                currentFragment = "settings";
                fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).replace(R.id.content_start, fragmentSettings);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();
            } /*else if (id == R.id.nav_share) {

            } else if (id == R.id.nav_send) {

            }*/
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void CheckSettingsClicked(View v) {
        EditText emailInput =  findViewById(R.id.settings_email_input);
        String email = emailInput.getText().toString();
        EditText passwordInput =  findViewById(R.id.settings_password);
        String password = passwordInput.getText().toString();
        this.user = email;
        this.password = password;
        getToken(email, password, true, null);
    }

    public void StartSynchronizationClicked(View v) {
        Cursor resultTimeKeepingsData = database.rawQuery("Select * from timekeepings WHERE synchronized=0", null);
        if (resultTimeKeepingsData != null && resultTimeKeepingsData.getCount() > 0) {
            resultTimeKeepingsData.moveToFirst();
            do {
                int eventType = resultTimeKeepingsData.getInt(resultTimeKeepingsData.getColumnIndex("eventType"));
                String comment = resultTimeKeepingsData.getString(resultTimeKeepingsData.getColumnIndex("comment"));
                String time = resultTimeKeepingsData.getString(resultTimeKeepingsData.getColumnIndex("time"));
                float duration = resultTimeKeepingsData.getFloat(resultTimeKeepingsData.getColumnIndex("duration"));
                final String dbUpateTime = time;
                int projectId = resultTimeKeepingsData.getInt(resultTimeKeepingsData.getColumnIndex("projectId"));
                PostTimeKeepData postTimeKeepData = new PostTimeKeepData(new PostTimeKeepData.PostTimeKeepDataListener() {
                    @Override
                    public void onFinished(String result) {
                        try {
                            JSONArray response = new JSONArray(result);
                            JSONObject innerObject = response.getJSONObject(0);
                            if (innerObject.getString("status").equals("ok")) {
                                SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
                                database.execSQL("update timekeepings set synchronized = 1 WHERE time = '" + dbUpateTime + "'");

                            }
                        } catch (Exception ex) {
                            Log.d("Error", ex.getMessage());

                        }
                    }
                }, token, eventType, comment, time, duration, projectId, false);
                postTimeKeepData.execute();
            }  while(resultTimeKeepingsData.moveToNext());

            resultTimeKeepingsData.close();
        }

        Cursor resultProjectImagesData = database.rawQuery("Select * from project_images WHERE synchronized=0", null);
        if (resultProjectImagesData != null && resultProjectImagesData.getCount() > 0) {
            resultProjectImagesData.moveToFirst();
            do {
                int id = resultProjectImagesData.getInt(resultProjectImagesData.getColumnIndex("id"));
                int projectId = resultProjectImagesData.getInt(resultProjectImagesData.getColumnIndex("project_id"));
                final String path = resultProjectImagesData.getString(resultProjectImagesData.getColumnIndex("path"));
                //we need to check if this file exists locally - if yes we post the image, if not we get it
                String imagePath = BaubegleiterApplication.getInstance().getImagePath(String.valueOf(projectId), path);
                File file = new File(imagePath);
                if (file.exists()) {
                    PostProjectImage postProjectImage = new PostProjectImage(new PostProjectImage.PostProjectImageListener() {
                        @Override
                        public void onFinished(String result) {
                            try {
                                JSONArray response = new JSONArray(result);
                                JSONObject innerObject = response.getJSONObject(0);
                                if (innerObject.getString("status").equals("ok")) {
                                    SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
                                    database.execSQL("update project_images set synchronized = 1 WHERE path = '" + path + "'");
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                                            builder.setMessage("Hochladen des Bildes fehlgeschlagen")
                                                    .setCancelable(false)
                                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            //do things
                                                        }
                                                    });
                                            AlertDialog alert = builder.create();
                                            alert.show();
                                        }
                                    });
                                }
                            } catch (Exception ex) {
                                Log.d("Error", ex.getMessage());
                            }
                        }
                    }, token, path, projectId, false);
                    postProjectImage.execute();
                } else {
                    getProjectImage(String.valueOf(id)+"#"+path+"#"+String.valueOf(projectId), String.valueOf(projectId));
                }
            }  while(resultProjectImagesData.moveToNext());

            resultProjectImagesData.close();
        }

        fragmentSynchronization.updateDataInView();
    }

    public void getProjects() {
        GetProjects getProjects = new GetProjects(new GetProjects.GetProjectsListener() {
            @Override
            public void onFinished(String[] projects) {
                //First we need to insert all already existing projects
                addProjectsFromDatabase();
                if (projects != null) {
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String startedUpdateTime = s.format(new Date());
                    boolean addedNewProject = false;
                    for (int i = 0; i < projects.length; i++) {
                        if (projects[i].isEmpty()) {
                            continue;
                        }

                        insertProjectToDatabase(projects[i]);
                        addedNewProject = true;

                        String project_string = projects[i].substring(projects[i].indexOf('-') + 2);
                        if (fragmentProjects != null) {
                            fragmentProjects.addItems(project_string);
                            fragmentTimeKeep.addItems(project_string, false);
                        }

                        String[] parts = projects[i].split(" - ");
                        BaubegleiterApplication.getInstance().makeDirectory(parts[0]);
                        updatePictureImages(projects[i]);
                    }

                    if (addedNewProject) {
                        database.execSQL("UPDATE synchronization SET timestamp='" + startedUpdateTime + "' WHERE name='projects'");
                    }
                }
            }
        }, token, lastProjectsSyncTime);
        getProjects.execute();
    }

    public void getTimekeepings() {
        GetTimekeepings getTimekeepings = new GetTimekeepings(new GetTimekeepings.GetTimekeepingsListener() {
            @Override
            public void onFinished(String[] timekeepings) {
                if (timekeepings != null) {
                    boolean addedNewTimekeep = false;
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String startedUpdateTime = s.format(new Date());
                    for (int i = 0; i < timekeepings.length; i++) {
                        if (timekeepings[i].isEmpty()) {
                            continue;
                        }

                        insertTimekeepToDatabase(timekeepings[i]);
                        addedNewTimekeep = true;
                    }

                    if (addedNewTimekeep) {
                        database.execSQL("UPDATE synchronization SET timestamp='" + startedUpdateTime + "' WHERE name='timekeepings'");
                    }
                }
            }
        }, token, lastTimeKeepingsSyncTime);
        getTimekeepings.execute();
    }

    private void addProjectsFromDatabase() {
        Cursor resultSetProjects = database.rawQuery("Select * from projects", null);
        if (resultSetProjects == null || !resultSetProjects.moveToFirst())
            return;
        do {
            String number = String.valueOf(resultSetProjects.getInt(resultSetProjects.getColumnIndex("number")));
            String customer = resultSetProjects.getString(resultSetProjects.getColumnIndex("customer"));
            String name = resultSetProjects.getString(resultSetProjects.getColumnIndex("name"));
            String project = number + " - " + customer + " - " + name;
            if (fragmentProjects != null) {
                fragmentProjects.addItems(project);
                fragmentTimeKeep.addItems(project, false);
            }
        }  while(resultSetProjects.moveToNext());

        resultSetProjects.close();
    }

    private void insertTimekeepToDatabase(String timekeep) {
        try {
            String fields[] = timekeep.split(";;");
            String eventType = fields[0];
            String time = fields[1];
            String comment = fields[2];
            Double duration = Double.parseDouble(fields[3]);
            String projectId = fields[4];
            String timekeeping_id = fields[5];

            SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();

            Cursor resultTimeKeepingsData = database.rawQuery("Select time from timekeepings WHERE time='"+ time +"'", null);
            if (resultTimeKeepingsData != null && resultTimeKeepingsData.getCount() > 0) {
                resultTimeKeepingsData.moveToFirst();
                String gotTime = resultTimeKeepingsData.getString(resultTimeKeepingsData.getColumnIndex("time"));
            } else {
                String insert = "insert or replace into timekeepings (event_id, time, comment, projectId, duration, synchronized) values (" + eventType + ", '" + time + "', '" + comment + "', '" + projectId + "', "+duration+", 1)";
                database.execSQL(insert);
            }
        } catch (Exception ex) {
            Log.d("baubegleiter", "Adding of timekeep data fails" + ex.getMessage());
            ex.printStackTrace();
        }
    }

    private void insertProjectToDatabase(String project) {
        try {
            String[] parts = project.split(" - ");
            int id = Integer.parseInt(parts[0]);
            int number = Integer.parseInt(parts[1]);
            String customer = parts[2];
            String name = parts[3];
            database.execSQL("insert or replace into projects (project_id, number, customer, name) values (" + id + ", " + number + ", '" + customer + "', '" + name + "')");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updatePictureImages(String picture) {
        String[] parts = picture.split(" - ");
        final String folderName = parts[0];
        final String projectId = parts[0];
        GetProjectImagesList getProjectImagesList = new GetProjectImagesList(new GetProjectImagesList.GetProjectsImagesListener() {
            @Override
            public void onFinished(String[] projects) {
                if (projects != null) {
                    for (int i = 0; i < projects.length; i++) {
                        if (!projects[i].isEmpty()) {
                            getProjectImage(projects[i], folderName);
                        }
                    }
                }
            }
        }, token, projectId, lastProjectsSyncTime);
        getProjectImagesList.execute();
    }

    private void getProjectImage(String pictureData, String folderName) {
        String[] parts = pictureData.split("#");
        final String fileName = parts[1];
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String startedUpdateTime = s.format(new Date());
        int prid = Integer.parseInt(parts[2]);
        database.execSQL("insert or replace into project_images (id, project_id, path, time, synchronized) values (null, "+prid+", '"+fileName+"', '"+startedUpdateTime+"', 0)");
        GetProjectImage getProjectImage = new GetProjectImage(new GetProjectImage.GetProjectImageListener() {
            @Override
            public void onFinished(String status) {
                if (status.equals("ok")) {
                    database.execSQL("UPDATE project_images SET synchronized = 1 WHERE path='"+fileName+"'");
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                    builder.setMessage("Fehler beim Laden eines Bildes aufgetreten, bitte App neu starten")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                            }
                        });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        }, token, parts[2], folderName, fileName);
        getProjectImage.execute();
    }

    public void saveUserPasswordData(String user, String password) {
        try {
            database = BaubegleiterApplication.getInstance().getDatabase();
            database.execSQL("UPDATE login SET user='" + user + "', password ='" + password + "'");

            //Check if we did call getProjects already - if not we need to do so otherwise we do not call it again here
            if (!getProjectsCalledOnStartup) {
                getProjects();
                getTimekeepings();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getToken(String user, String password, boolean forceGetToken, final FinishGetUserPasswordAndGotToken finishGetUserPasswordAndGotToken) {
        final boolean updateSettingsResult = forceGetToken;
        final String userString = user;
        final String passwordString = password;
        if (forceGetToken || !isDateTimeValid()) {
            GetToken getToken = new GetToken(new GetToken.GetTokenListener() {
                @Override
                public void onFinished(String tokenVal, String expiresVal) {
                    Log.d("Token", tokenVal);
                    token = tokenVal;
                    if (token != "") {
                        saveUserPasswordData(userString, passwordString);
                    }

                    tokenExpires = expiresVal;
                    if (updateSettingsResult) {
                        TextView result = findViewById(R.id.settings_result);
                        if (result != null) {
                            if (token != "") {
                                result.setText("Eingaben gültig bis: " + tokenExpires);
                            } else {
                                result.setText("Benutzer und Passwort stimmen nicht");
                            }
                        }
                    }

                    if (finishGetUserPasswordAndGotToken != null) {
                        finishGetUserPasswordAndGotToken.onFinished();
                    }
                }
            }, user, password);
            getToken.execute();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_PICTURE) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d("File", imageFilePath);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // User Cancelled the action
            }
        }
    }

    private void openCameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        pictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        if(pictureIntent.resolveActivity(getPackageManager()) != null){
            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }

            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.lagetar.baubegleiter.provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                startActivityForResult(pictureIntent,
                        TAKE_PICTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        imageFileName = imageFileName + ".jpg";
        return image;
    }
}
