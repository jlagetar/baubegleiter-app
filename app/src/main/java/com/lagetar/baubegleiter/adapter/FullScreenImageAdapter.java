package com.lagetar.baubegleiter.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.lagetar.baubegleiter.BaubegleiterApplication;
import com.lagetar.baubegleiter.R;
import com.lagetar.baubegleiter.helper.TouchImageView;

import java.io.File;
import java.util.ArrayList;

public class FullScreenImageAdapter extends PagerAdapter {

	private Activity _activity;
	private ArrayList<String> _imagePaths;
	private LayoutInflater inflater;

	// constructor
	public FullScreenImageAdapter(Activity activity,
			ArrayList<String> imagePaths) {
		this._activity = activity;
		this._imagePaths = imagePaths;
	}

	@Override
	public int getCount() {
		return this._imagePaths.size();
	}

	@Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }
	
	@Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);

		TouchImageView imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
		Button btnClose = (Button) viewLayout.findViewById(R.id.btnClose);
		Button btnDelete = (Button) viewLayout.findViewById(R.id.btnDelete);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		final String imagePath = _imagePaths.get(position);
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
        imgDisplay.setImageBitmap(bitmap);
        
        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				_activity.finish();
			}
		});

		btnDelete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			    File file = new File(imagePath);
			    String fileName = file.getName();
                if (file.delete()) {
                    try {
                        SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
                        database.execSQL("DELETE FROM project_images WHERE path = '" + fileName + "'");
                        _activity.finish();
                        //FIXME synchronize this to the server
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
			}
		});

        ((ViewPager) container).addView(viewLayout);
 
        return viewLayout;
	}
	
	@Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}
