package com.lagetar.baubegleiter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.lagetar.baubegleiter.adapter.GridViewImageAdapter;
import com.lagetar.baubegleiter.helper.AppConstant;
import com.lagetar.baubegleiter.helper.Utils;
import com.lagetar.baubegleiter.webrequest.PostProjectImage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class GridViewActivity extends Activity {
	private Utils utils;
	private ArrayList<String> imagePaths = new ArrayList<String>();
	private GridViewImageAdapter adapter;
	private GridView gridView;
	private int columnWidth;
	private String token;
	private String projectId;
    public static final String ANDROID_CHANNEL_ID = "com.lagetar.baubegleiter.ANDROID";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_grid_view);

		gridView = (GridView) findViewById(R.id.grid_view);

        Intent i = getIntent();
        token = i.getStringExtra("token");
        projectId = i.getStringExtra("project");
		utils = new Utils(this, projectId);

		// Initilizing Grid View
		InitilizeGridLayout();

		// loading all image paths from SD card
		imagePaths = utils.getFilePaths();

		// Gridview adapter
		adapter = new GridViewImageAdapter(GridViewActivity.this, imagePaths,
				columnWidth);

		// setting grid view adapter
		gridView.setAdapter(adapter);
		gridView.setClickable(true);
		gridView.setFocusable(false);
		gridView.setFocusableInTouchMode(false);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                if (position > 0) {
                    Intent i = new Intent(GridViewActivity.this, FullScreenViewActivity.class);
                    i.putExtra("position", position);
                    i.putExtra("project", projectId);
                    //GridViewActivity.this.startActivity(i);
                    startActivityForResult(i, Start.SHOW_FULLCREEN_PICTURE);
                } else {
                    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                    StrictMode.setVmPolicy(builder.build());

                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    startActivityForResult(Intent.createChooser(intent, "Bild auswählen"), Start.PICK_IMAGE);
                }
            }
        });
    }


    public void copy(Uri src, File dst) throws IOException {
        try (InputStream in = getContentResolver().openInputStream(src)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Start.PICK_IMAGE) {
            if (data != null) {
                if(resultCode == Activity.RESULT_OK) {
                    if(data.getClipData() != null) {
                        int count = data.getClipData().getItemCount();
                        int currentItem = 0;
                        while(currentItem < count) {
                            Uri imageUri = data.getClipData().getItemAt(currentItem).getUri();
                            currentItem = currentItem + 1;
                            startUploadImage(imageUri, currentItem);
                        }
                    } else if(data.getData() != null) {
                        startUploadImage(data.getData(), 1);
                    }

                    if (adapter != null ) {
                        adapter.notifyDataSetChanged();
                        imagePaths = utils.getFilePaths();
                        adapter = new GridViewImageAdapter(GridViewActivity.this, imagePaths,
                                columnWidth);
                        gridView.invalidateViews();
                        gridView.setAdapter(adapter);
                    }
                }
            }
        } else if (requestCode == Start.SHOW_FULLCREEN_PICTURE) {
            if (adapter != null ) {
                adapter.notifyDataSetChanged();
                imagePaths = utils.getFilePaths();
                adapter = new GridViewImageAdapter(GridViewActivity.this, imagePaths,
                        columnWidth);
                gridView.invalidateViews();
                gridView.setAdapter(adapter);
            }
        }
    }

    private void startUploadImage(Uri sourceFilePath, int index) {
        try
        {
            final NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            Random r = new Random();
            final int notificationId = r.nextInt(1000000);
            final NotificationCompat.Builder builder = new NotificationCompat.Builder(this, ANDROID_CHANNEL_ID);
            builder.setContentTitle("Bild"+index+" hochladen")
                    .setContentText("Vorgang läuft")
                    .setSmallIcon(R.drawable.ic_notification)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

// Issue the initial notification with zero progress
            int PROGRESS_MAX = 100;
            int PROGRESS_CURRENT = 0;
            builder.setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false);
            notificationManager.notify(notificationId, builder.build());

            String timeStamp =
                    new SimpleDateFormat("yyyyMMdd_HHmmss"+index,
                            Locale.getDefault()).format(new Date());

            final String imageFileName = "IMG_" + timeStamp + ".jpg";
            Log.d("data", sourceFilePath.toString());
            Log.d("File", imageFileName);
            final String destinationPath = BaubegleiterApplication.getInstance().getImagePath(projectId, imageFileName);
            final File destinationWrite = new File(destinationPath);

            destinationWrite.createNewFile();
            copy(sourceFilePath, destinationWrite);
            saveBitmapToFile(destinationWrite);

            PostProjectImage postProjectImage = new PostProjectImage(new PostProjectImage.PostProjectImageListener() {
                @Override
                public void onFinished(String result) {
                    try {
                        JSONArray response = new JSONArray(result);
                        JSONObject innerObject = response.getJSONObject(0);
                        if (innerObject.getString("status").equals("ok")) {
                            SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
                            database.execSQL("update project_images set synchronized = 1 WHERE path = '" + imageFileName + "'");
                           //Redraw images here
                            if (adapter != null ) {
                                adapter.notifyDataSetChanged();
                                imagePaths = utils.getFilePaths();
                                adapter = new GridViewImageAdapter(GridViewActivity.this, imagePaths,
                                        columnWidth);
                                gridView.invalidateViews();
                                gridView.setAdapter(adapter);
                            }
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                                    builder.setMessage("Hochladen des Bildes fehlgeschlagen")
                                            .setCancelable(false)
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //do things
                                                }
                                            });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                }
                            });
                        }
                    } catch(Exception ex) {
                        Log.d("Error", ex.getMessage());
                    }

                    builder.setContentText("Hochladen fertig")
                            .setProgress(0,0,false);
                    notificationManager.notify(notificationId, builder.build());
                }
            }, token, imageFileName, Integer.parseInt(projectId), true);
            postProjectImage.execute();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private File saveBitmapToFile(File file) {
        try {
            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE=75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return response.toString();
    }

	private void InitilizeGridLayout() {
		Resources r = getResources();
		float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				AppConstant.GRID_PADDING, r.getDisplayMetrics());

		columnWidth = (int) ((utils.getScreenWidth() - ((AppConstant.NUM_OF_COLUMNS + 1) * padding)) / AppConstant.NUM_OF_COLUMNS);

		gridView.setNumColumns(AppConstant.NUM_OF_COLUMNS);
		gridView.setColumnWidth(columnWidth);
		gridView.setStretchMode(GridView.NO_STRETCH);
		gridView.setPadding((int) padding, (int) padding, (int) padding,
				(int) padding);
		gridView.setHorizontalSpacing((int) padding);
		gridView.setVerticalSpacing((int) padding);
	}
}
