package com.lagetar.baubegleiter;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.io.IOException;

public class BaubegleiterApplication extends BApplication {
    private SQLiteDatabase database = null;
    private DatabaseHelper myDbHelper = null;
    private String currentVersion = null;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        closeDatabase();
    }

    @Override
    public void setVersion(String version) {
        this.currentVersion = version;
    }

    @Override
    public String getVersion() {
        return this.currentVersion;
    }

    @Override
    public String getImagePath(String folder, String imageName) {
        try {
            String directory = makeDirectory(folder);
            File mypath = new File(directory, imageName);
            return mypath.getAbsolutePath();
        } catch(Exception e) {
            e.printStackTrace();
            Log.d("Error", "Error creating file");
        }

        return null;
    }

    @Override
    public String[] getImagesInFolder(String folder) {
        try {
            File folderDir =  new File(getApplicationContext().getFilesDir(), folder);
            if (folderDir != null) {
                return folderDir.list();
            }
        } catch(Exception e) {
            e.printStackTrace();
            Log.d("Error", "Error reading file");
        }

        return null;
    }

    @Override
    public String makeDirectory(String directoryName) {
        File profileDirectory = new File(getApplicationContext().getFilesDir(), directoryName);
        profileDirectory.mkdir();
        return profileDirectory.getAbsolutePath();
    }

    @Override
    public synchronized SQLiteDatabase getDatabase() throws IOException {
        if (database == null) {
            if (myDbHelper == null) {
                myDbHelper = new DatabaseHelper(this.getApplicationContext());
            }

            if (myDbHelper.getMyDataBase() == null) {
                try {
                    myDbHelper.createDataBase();
                } catch (IOException ioe) {
                    throw new Error("Unable to create database");
                }
            }

            try {
                myDbHelper.openDataBase();
            } catch(Exception sqle){
                sqle.printStackTrace();
            }

            database = myDbHelper.getMyDataBase();
        }

        Log.d("DATABASE", "Opened database");
        return database;
    }

    @Override
    public void closeDatabase() {
        try {
            if (database != null) {
                database.close();
                database = null;
            }
        } catch (Exception e) {
            throw new Error("Unable to close database");
        }

        Log.d("DATABASE", "Closed database");
    }
}
