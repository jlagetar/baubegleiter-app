package com.lagetar.baubegleiter.webrequest;

import android.os.AsyncTask;

import com.lagetar.baubegleiter.BaubegleiterApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetProjectImagesList extends AsyncTask<String, Integer, String> {
    public interface GetProjectsImagesListener {
        void onFinished(String[] projects);
    }

    private final GetProjectsImagesListener getProjectsImagesListener;
    private String tokenValue = null;
    private String projectIdValue = null;
    private String syncTimeValue = null;

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    public GetProjectImagesList(GetProjectsImagesListener getProjectsImagesListener, String token, String projectId, String lastProjectsUpdateTime) {
        this.getProjectsImagesListener = getProjectsImagesListener;
        this.tokenValue = token;
        this.projectIdValue = projectId;
        this.syncTimeValue = lastProjectsUpdateTime;
        if (syncTimeValue == null) {
            syncTimeValue = "2018-09-01 00:00:00";
        }
    }

    @Override
    protected String doInBackground(String... stringUpdate) {
        try {
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("token", tokenValue);
            jsonParam.put("type", "project_images");
            jsonParam.put("time", syncTimeValue);
            jsonParam.put("project_id", projectIdValue);
            jsonParam.put("version", BaubegleiterApplication.getInstance().getVersion());

            URL url = new URL(BaubegleiterApplication.getWebUrl() + "app/get_data.php?jsondata="+jsonParam.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);

            conn.setDoInput(true);

            InputStream in = conn.getInputStream();
            String responseString = readStream(in);
            in.close();
            conn.disconnect();

            JSONArray response = new JSONArray(responseString);
            String projectsString = "";
            for (int arrIndex = 0; arrIndex < response.length(); arrIndex++) {
                JSONObject innerObject = response.getJSONObject(arrIndex);
                if (arrIndex == 0) {
                    if (innerObject.getString("status").equals("invalid_token")) {
                        return null;
                    }
                } else {
                    JSONArray projects = innerObject.getJSONArray("project_images");
                    for (int i = 0; i < projects.length(); i++) {
                        String id = projects.getJSONObject(i).getString("id");
                        String path = projects.getJSONObject(i).getString("path");
                        String project_id = projects.getJSONObject(i).getString("project_id");
                        String data = id + "#" + path + "#" + project_id;
                        if (i > 0 ) {
                            projectsString = projectsString + "##";
                        }

                        projectsString = projectsString + data;
                    }
                }
            }

            return projectsString;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String httpResponse) {
        super.onPostExecute(httpResponse);
        if (httpResponse != null) {
            String data[] = httpResponse.split("##");
            this.getProjectsImagesListener.onFinished(data);
        } else {
            this.getProjectsImagesListener.onFinished(null);
        }
    }
}
