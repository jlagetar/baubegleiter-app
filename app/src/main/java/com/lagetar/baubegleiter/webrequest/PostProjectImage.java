package com.lagetar.baubegleiter.webrequest;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Base64;

import com.lagetar.baubegleiter.BaubegleiterApplication;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PostProjectImage extends AsyncTask<String, Integer, String> {
    public interface PostProjectImageListener {
        void onFinished(String result);
    }

    private final PostProjectImageListener postProjectImageListener;
    private String token;
    private String imagePath;
    private boolean addToDatabase;
    private int projectId;

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    public PostProjectImage(PostProjectImage.PostProjectImageListener postProjectImageListener, String token, String imagePath, int projectId, boolean addToDatabase) {
        this.postProjectImageListener = postProjectImageListener;
        this.token = token;
        this.imagePath = imagePath;
        this.projectId = projectId;
        this.addToDatabase = addToDatabase;
    }

    @Override
    protected String doInBackground(String... stringUpdate) {
        try {
            if (this.addToDatabase) {
                SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String startedUpdateTime = s.format(new Date());
                database.execSQL("insert or replace into project_images (id, project_id, path, time, synchronized) values (null, "+projectId+", '"+imagePath+"', '"+startedUpdateTime+"', 0)");
            }

            final String destinationPath = BaubegleiterApplication.getInstance().getImagePath(String.valueOf(projectId), imagePath);
            final File destinationWrite = new File(destinationPath);

            int size = (int) destinationWrite.length();
            byte[] bytes = new byte[size];
            try {
                BufferedInputStream buf = new BufferedInputStream(new FileInputStream(destinationWrite));
                buf.read(bytes, 0, bytes.length);
                buf.close();
                String image_str = Base64.encodeToString(bytes, Base64.NO_WRAP | Base64.URL_SAFE);
                String postParameters = "token="+token;
                postParameters = postParameters + "&type=image_upload";
                postParameters = postParameters + "&project_id=" + String.valueOf(projectId);
                postParameters = postParameters + "&image="+image_str;
                postParameters = postParameters + "&name="+imagePath;
                postParameters = postParameters + "&version="+BaubegleiterApplication.getInstance().getVersion();

                URL url = new URL(BaubegleiterApplication.getWebUrl() + "app/post_data.php");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setUseCaches(false);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("Cache-Control", "no-cache");
                conn.setRequestProperty("Content-Length", postParameters);
                conn.setConnectTimeout(100000);
                conn.setReadTimeout(100000);
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                DataOutputStream out = new DataOutputStream(conn.getOutputStream());
                out.writeBytes(postParameters);
                out.flush();
                out.close();

                InputStream in = conn.getInputStream();
                String responseString = readStream(in);
                in.close();
                conn.disconnect();
                return responseString;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String httpResponse) {
        super.onPostExecute(httpResponse);
        this.postProjectImageListener.onFinished(httpResponse);
    }
}
