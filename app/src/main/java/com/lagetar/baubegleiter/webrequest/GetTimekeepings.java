package com.lagetar.baubegleiter.webrequest;

import android.os.AsyncTask;

import com.lagetar.baubegleiter.BaubegleiterApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetTimekeepings extends AsyncTask<String, Integer, String> {
    public interface GetTimekeepingsListener {
        void onFinished(String[] projects);
    }

    private final GetTimekeepingsListener getTimekeepingsListener;
    private String tokenValue = null;
    private String lastSyncTime = null;

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    public GetTimekeepings(GetTimekeepingsListener getTimekeepingsListener, String token, String lastSyncTime) {
        this.getTimekeepingsListener = getTimekeepingsListener;
        this.tokenValue = token;
        this.lastSyncTime = lastSyncTime;
    }

    @Override
    protected String doInBackground(String... stringUpdate) {
        try {
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("token", tokenValue);
            jsonParam.put("type", "timkeepings");
            jsonParam.put("time", lastSyncTime);
            jsonParam.put("version", BaubegleiterApplication.getInstance().getVersion());

            URL url = new URL(BaubegleiterApplication.getWebUrl() + "app/get_data.php?jsondata="+jsonParam.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);

            conn.setDoInput(true);

            InputStream in = conn.getInputStream();
            String responseString = readStream(in);
            in.close();
            conn.disconnect();

            JSONArray response = new JSONArray(responseString);
            String timekeepingsString = "";
            for (int arrIndex = 0; arrIndex < response.length(); arrIndex++) {
                JSONObject innerObject = response.getJSONObject(arrIndex);
                if (arrIndex == 0) {
                    if (innerObject.getString("status").equals("invalid_token")) {
                        return null;
                    }
                } else {
                    JSONArray timekeepings = innerObject.getJSONArray("timekeepings");
                    for (int i = 0; i < timekeepings.length(); i++) {
                        String event_id = timekeepings.getJSONObject(i).getString("event_id");
                        String time = timekeepings.getJSONObject(i).getString("time");
                        String comment = timekeepings.getJSONObject(i).getString("comment");
                        String duration = timekeepings.getJSONObject(i).getString("duration");
                        String project_id = timekeepings.getJSONObject(i).getString("project_id");
                        String timekeeping_id = timekeepings.getJSONObject(i).getString("timekeeping_id");
                        String singleLine = event_id + ";;" + time + ";;" + comment + ";;" + duration + ";;" + project_id + ";;" + timekeeping_id;

                        if (i > 0 ) {
                            timekeepingsString = timekeepingsString + "##";
                        }

                        timekeepingsString = timekeepingsString + singleLine;
                    }
                }
            }

            return timekeepingsString;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String httpResponse) {
        super.onPostExecute(httpResponse);
        if (httpResponse != null) {
            String data[] = httpResponse.split("##");
            this.getTimekeepingsListener.onFinished(data);
        } else {
            this.getTimekeepingsListener.onFinished(null);
        }
    }
}
