package com.lagetar.baubegleiter.webrequest;

import android.os.AsyncTask;
import android.util.Base64;

import com.lagetar.baubegleiter.BaubegleiterApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class GetToken extends AsyncTask<String, Integer, String> {
    private static String SHA256 (String text) throws NoSuchAlgorithmException {
        try {
            String secret_key = "69ba594657c6ba1e08763f73c204b40f";
            String secret_iv = "a7c0238345bf7efa";
            SecretKeySpec skeySpec = new SecretKeySpec(secret_key.getBytes() , "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(secret_iv.getBytes());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            //solved using PRNGFixes class
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);
            return Base64.encodeToString(cipher.doFinal(text.getBytes()), Base64.NO_WRAP | Base64.URL_SAFE);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public interface GetTokenListener {
        void onFinished(String token, String expires);
    }

    private final GetTokenListener getTokenListener;
    private String userValue = null;
    private String passwordValue = null;

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    public GetToken(GetTokenListener getTokenListener, String user, String password) {
        this.getTokenListener = getTokenListener;
        this.userValue = user;
        this.passwordValue = password;
    }

    @Override
    protected String doInBackground(String... stringUpdate) {
        try {
            String hashedPass = SHA256(passwordValue);
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("username", userValue);
            jsonParam.put("password", hashedPass);
            jsonParam.put("type", "get_token");
            jsonParam.put("version", BaubegleiterApplication.getInstance().getVersion());

            URL url = new URL(BaubegleiterApplication.getWebUrl() + "app/get_data.php?jsondata=" + jsonParam.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);

            conn.setDoInput(true);

            InputStream in = conn.getInputStream();
            String responseString = readStream(in);
            in.close();
            conn.disconnect();

            JSONArray response = new JSONArray(responseString);
            String token = "";
            String expires = "";
            for (int arrIndex = 0; arrIndex < response.length(); arrIndex++) {
                JSONObject innerObject = response.getJSONObject(arrIndex);
                if (arrIndex == 0) {
                    if (innerObject.getString("status").equals("invalid_token")) {
                        break;
                    }
                } else if (arrIndex == 1) {
                    token = innerObject.getString("token");
                } else {
                    expires = innerObject.getString("expires");
                }
            }

            return token + "##" + expires;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String httpResponse) {
        super.onPostExecute(httpResponse);
        if (httpResponse != null) {
            String data[] = httpResponse.split("##");
            this.getTokenListener.onFinished(data[0], data[1]);
        } else {
            this.getTokenListener.onFinished("", "");
        }
    }
}
