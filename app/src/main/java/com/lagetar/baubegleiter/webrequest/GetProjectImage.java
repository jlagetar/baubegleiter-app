package com.lagetar.baubegleiter.webrequest;

import android.os.AsyncTask;

import com.lagetar.baubegleiter.BaubegleiterApplication;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

public class GetProjectImage extends AsyncTask<String, Integer, String> {
    public interface GetProjectImageListener {
        void onFinished(String status);
    }

    private final GetProjectImageListener getProjectImageListener;
    private String tokenValue = null;
    private String projectIdValue = null;
    private String folderName = null;
    private String fileName = null;

    public GetProjectImage(GetProjectImageListener getProjectImageListener, String token, String projectId, String folderName, String fileName) {
        this.getProjectImageListener = getProjectImageListener;
        this.tokenValue = token;
        this.projectIdValue = projectId;
        this.folderName = folderName;
        this.fileName = fileName;
    }

    @Override
    protected String doInBackground(String... stringUpdate) {
        try {
            URL url = new URL(BaubegleiterApplication.getWebUrl() + "server/php/files/"+projectIdValue+"/" + fileName);
            String path = BaubegleiterApplication.getInstance().getImagePath(folderName, fileName);
            if (path != null) {
                InputStream in = new BufferedInputStream(url.openStream());
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                byte[] buf = new byte[1024];
                int n = in.read(buf);
                while (n != -1) {
                    out.write(buf, 0, n);
                    n = in.read(buf);
                }
                out.close();
                in.close();
                byte[] response = out.toByteArray();

                FileOutputStream fos = new FileOutputStream(path);
                fos.write(response);
                fos.close();
                return "ok";
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String httpResponse) {
        super.onPostExecute(httpResponse);
        this.getProjectImageListener.onFinished(httpResponse);
    }
}
