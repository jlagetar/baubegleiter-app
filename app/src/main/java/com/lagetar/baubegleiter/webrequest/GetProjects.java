package com.lagetar.baubegleiter.webrequest;

import android.os.AsyncTask;

import com.lagetar.baubegleiter.BaubegleiterApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetProjects extends AsyncTask<String, Integer, String> {
    public interface GetProjectsListener {
        void onFinished(String[] projects);
    }

    private final GetProjectsListener getProjectsListener;
    private String tokenValue = null;
    private String lastSyncTime = null;

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    public GetProjects(GetProjectsListener getProjectsListener, String token, String lastSyncTime) {
        this.getProjectsListener = getProjectsListener;
        this.tokenValue = token;
        this.lastSyncTime = lastSyncTime;
    }

    @Override
    protected String doInBackground(String... stringUpdate) {
        try {
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("token", tokenValue);
            jsonParam.put("type", "projects");
            jsonParam.put("time", lastSyncTime);
            jsonParam.put("version", BaubegleiterApplication.getInstance().getVersion());

            URL url = new URL(BaubegleiterApplication.getWebUrl() + "app/get_data.php?jsondata="+jsonParam.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);

            conn.setDoInput(true);

            InputStream in = conn.getInputStream();
            String responseString = readStream(in);
            in.close();
            conn.disconnect();

            JSONArray response = new JSONArray(responseString);
            String projectsString = "";
            for (int arrIndex = 0; arrIndex < response.length(); arrIndex++) {
                JSONObject innerObject = response.getJSONObject(arrIndex);
                if (arrIndex == 0) {
                    if (innerObject.getString("status").equals("invalid_token")) {
                        return null;
                    }
                } else {
                    JSONArray projects = innerObject.getJSONArray("projects");
                    for (int i = 0; i < projects.length(); i++) {
                        String project_id = projects.getJSONObject(i).getString("id");
                        String number = projects.getJSONObject(i).getString("number");
                        String name = projects.getJSONObject(i).getString("name");
                        String customer = projects.getJSONObject(i).getString("customer");
                        String path = project_id + " - " + number + " - " + name + " - " + customer;
                        if (i > 0 ) {
                            projectsString = projectsString + "##";
                        }

                        projectsString = projectsString + path;
                    }
                }
            }

            return projectsString;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String httpResponse) {
        super.onPostExecute(httpResponse);
        if (httpResponse != null) {
            String data[] = httpResponse.split("##");
            this.getProjectsListener.onFinished(data);
        } else {
            this.getProjectsListener.onFinished(null);
        }
    }
}
