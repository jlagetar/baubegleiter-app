package com.lagetar.baubegleiter.webrequest;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.lagetar.baubegleiter.BaubegleiterApplication;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class PostTimeKeepData extends AsyncTask<String, Integer, String> {
    public interface PostTimeKeepDataListener {
        void onFinished(String result);
    }

    private final PostTimeKeepDataListener postTimeKeepDataListener;
    private String token;
    private int eventType;
    private String comment;
    private String time;
    private boolean addToDatabase;
    private int projectId;
    private float duration;

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    public PostTimeKeepData(PostTimeKeepData.PostTimeKeepDataListener postTimeKeepDataListener, String token, int eventType, String comment, String time, float duration, int projectId, boolean addToDatabase) {
        this.postTimeKeepDataListener = postTimeKeepDataListener;
        this.token = token;
        this.eventType = eventType;
        this.comment = comment;
        this.time = time;
        this.projectId = projectId;
        this.addToDatabase = addToDatabase;
        this.duration = duration;
    }

    @Override
    protected String doInBackground(String... stringUpdate) {
        try {
            if (this.addToDatabase) {
                SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
                database.execSQL("insert or replace into timekeepings (event_id, time, comment, projectId, duration, synchronized) values ("+eventType+", '"+time+"', "+ projectId+ ", '"+comment+"', "+duration+", 0)");
            }

            String postParameters = "token="+token;
            postParameters = postParameters + "&type=timekeep";
            postParameters = postParameters + "&eventtype=" + eventType;
            postParameters = postParameters + "&time="+time;
            postParameters = postParameters + "&comment="+comment;
            postParameters = postParameters + "&duration="+duration;
            postParameters = postParameters + "&projectId="+projectId;
            postParameters = postParameters + "&version="+BaubegleiterApplication.getInstance().getVersion();

            URL url = new URL(BaubegleiterApplication.getWebUrl() + "app/post_data.php");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setUseCaches(false);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Cache-Control", "no-cache");
            conn.setRequestProperty("Content-Length", postParameters);
            conn.setConnectTimeout(100000);
            conn.setReadTimeout(100000);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            DataOutputStream out = new DataOutputStream(conn.getOutputStream());
            out.writeBytes(postParameters);
            out.flush();
            out.close();

            InputStream in = conn.getInputStream();
            String responseString = readStream(in);
            in.close();
            conn.disconnect();
            return responseString;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String httpResponse) {
        super.onPostExecute(httpResponse);
        this.postTimeKeepDataListener.onFinished(httpResponse);
    }
}
