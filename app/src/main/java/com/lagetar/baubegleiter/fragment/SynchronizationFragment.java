package com.lagetar.baubegleiter.fragment;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.lagetar.baubegleiter.BaubegleiterApplication;
import com.lagetar.baubegleiter.R;

public class SynchronizationFragment extends Fragment {
    private TextView synchronizationText = null;
    private Button synchronizationButton = null;

    public static Fragment newInstance(Context context) {
        SynchronizationFragment f = new SynchronizationFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.synchronization, null);
        synchronizationText = (TextView) root.findViewById(R.id.synchronization_items);
        synchronizationButton = (Button) root.findViewById(R.id.synchronization_button);
        updateDataInView();

        return root;
    }

    public void updateDataInView() {
        if (synchronizationText != null && synchronizationButton != null) {
            try {
                SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
                String timeSynchronizedCountMissing = "";
                String projectImgesSynchronizedCountMissing = "";

                Cursor resultTimeKeepingsData = database.rawQuery("Select COUNT(*) as count from timekeepings WHERE synchronized=0", null);
                if (resultTimeKeepingsData != null && resultTimeKeepingsData.getCount() > 0) {
                    resultTimeKeepingsData.moveToFirst();
                    timeSynchronizedCountMissing = resultTimeKeepingsData.getString(resultTimeKeepingsData.getColumnIndex("count"));
                }

                Cursor resultProjectImagesData = database.rawQuery("Select COUNT(*) as count from project_images WHERE synchronized=0", null);
                if (resultProjectImagesData != null && resultProjectImagesData.getCount() > 0) {
                    resultProjectImagesData.moveToFirst();
                    projectImgesSynchronizedCountMissing = resultProjectImagesData.getString(resultProjectImagesData.getColumnIndex("count"));
                }

                if ((timeSynchronizedCountMissing .isEmpty() || timeSynchronizedCountMissing.equals("0")) &&
                        (projectImgesSynchronizedCountMissing.isEmpty() || projectImgesSynchronizedCountMissing.equals("0"))) {
                    synchronizationText.setText("Alles synchronisiert");
                    synchronizationButton.setEnabled(false);
                } else {
                    synchronizationButton.setEnabled(true);
                    synchronizationText.setText(timeSynchronizedCountMissing + " Zeiten und " + projectImgesSynchronizedCountMissing + " Projekt-Bilder müssen noch synchronisiert werden");
                }
            } catch(Exception ex) {
                Log.d("DATABASE", ex.getMessage());
            }
        }
    }
}
