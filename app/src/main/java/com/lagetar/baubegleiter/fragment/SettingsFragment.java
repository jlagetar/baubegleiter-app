package com.lagetar.baubegleiter.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lagetar.baubegleiter.R;

public class SettingsFragment extends Fragment {

    public static Fragment newInstance(Context context) {
        SettingsFragment f = new SettingsFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.settings, null);
        Bundle bundle = getArguments();
        if (bundle != null) {
            String user = bundle.getString("user");
            String password = bundle.getString("password");
            TextView emailText = root.findViewById(R.id.settings_email_input);
            if (emailText != null) {
                emailText.setText(user);
            }

            TextView passwordText = root.findViewById(R.id.settings_password);
            if (passwordText != null) {
                passwordText.setText(password);
            }
        }
        return root;
    }
}
