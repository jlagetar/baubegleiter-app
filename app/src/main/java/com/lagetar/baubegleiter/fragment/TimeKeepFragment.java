package com.lagetar.baubegleiter.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.lagetar.baubegleiter.BaubegleiterApplication;
import com.lagetar.baubegleiter.R;
import com.lagetar.baubegleiter.domain.Event;
import com.lagetar.baubegleiter.helper.BauSpinner;
import com.lagetar.baubegleiter.webrequest.PostTimeKeepData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class TimeKeepFragment extends Fragment {
    private String token;
    ArrayList<String> listItems=new ArrayList<String>();
    private ArrayAdapter<String> adapter = null;
    private List<String> itemstoAdd = new ArrayList<String>();
    private String currentState = "";
    private TextView clickedStepResultText;
    private BauSpinner dropdown;
    private BauSpinner projects;
    private String[] timekeep_array;
    private BauSpinner durationHour;
    private BauSpinner durationMinutes;
    private TextView commentView;
    private TextView headerText;
    private TextView errorResultView;
    private ProgressBar progressBar;
    private CheckBox checkBox;
    private DatePicker datePicker;
    private DatePicker date_picker2;
    private TimePicker timePicker;
    private Button submitButton;
    private Button timeSubmitButton;
    private boolean isDateAndTimeSetFromCalendar = false;
    private String setDateFromCalendar = "";
    private int setTimeFromCalendar = 0;
    private Event event = null;

    public static Fragment newInstance(Context context) {
        TimeKeepFragment f = new TimeKeepFragment();
        return f;
    }

    public void setChangeEvent(Event event) {
        this.event = event;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.timekeep, null);
        this.dropdown = (BauSpinner) root.findViewById(R.id.spinner);
        dropdown.setVisibility(View.VISIBLE);
        this.projects = (BauSpinner) root.findViewById(R.id.projects);
        projects.setVisibility(View.INVISIBLE);
        this.timekeep_array = getResources().getStringArray(R.array.timekeep_array);
        this.durationHour = (BauSpinner) root.findViewById(R.id.durationHour);
        durationHour.setVisibility(View.INVISIBLE);
        this.durationMinutes = (BauSpinner) root.findViewById(R.id.durationMinutes);
        durationMinutes.setVisibility(View.INVISIBLE);

        ArrayAdapter<CharSequence> hourAdapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.hour_array, android.R.layout.simple_spinner_dropdown_item);

        durationHour.setAdapter(hourAdapter);

        ArrayAdapter<CharSequence> minutesAdapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.minutes_array, android.R.layout.simple_spinner_dropdown_item);

        durationMinutes.setAdapter(minutesAdapter);

        this.commentView = root.findViewById(R.id.comment);
        commentView.setVisibility(View.INVISIBLE);
        this.clickedStepResultText = root.findViewById(R.id.clickedStepResultText);
        clickedStepResultText.setVisibility(View.VISIBLE);
        clickedStepResultText.setKeyListener(null);
        this.headerText = root.findViewById(R.id.headerText);
        headerText.setText("Was soll eingetragen werden?");
        headerText.setVisibility(View.VISIBLE);
        headerText.setKeyListener(null);
        this.errorResultView = root.findViewById(R.id.errorResult);
        errorResultView.setVisibility(View.INVISIBLE);
        errorResultView.setKeyListener(null);
        this.progressBar = root.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        this.checkBox = root.findViewById(R.id.checkBox);
        checkBox.setVisibility(View.INVISIBLE);
        ArrayAdapter<CharSequence> adapterDropdown = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),
                R.array.timekeep_array, android.R.layout.simple_spinner_dropdown_item);
        dropdown.setAdapter(adapterDropdown);

        adapter =  new ArrayAdapter<String>(getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_1,
                listItems);

        if (itemstoAdd.size() > 0) {
            for (int i=0; i < itemstoAdd.size(); i++) {
                addItems(itemstoAdd.get(i), false);
            }
        }

        addItems("0-Allgemein", true);
        addItems("Auswählen...", true);

        projects.setAdapter(adapter);

        this.datePicker = root.findViewById(R.id.date_picker);
        datePicker.setVisibility(View.INVISIBLE);
        this.date_picker2 = root.findViewById(R.id.date_picker2);
        date_picker2.setVisibility(View.INVISIBLE);
        this.timePicker = root.findViewById(R.id.time_picker);
        timePicker.setVisibility(View.INVISIBLE);
        timePicker.setIs24HourView(true);
        if (isDateAndTimeSetFromCalendar) {
            timePicker.setCurrentHour(setTimeFromCalendar);
            timePicker.setCurrentMinute(0);
        }

        this.submitButton = root.findViewById(R.id.submit);
        submitButton.setVisibility(View.INVISIBLE);
        this.timeSubmitButton = root.findViewById(R.id.timeSubmit);
        timeSubmitButton.setVisibility(View.INVISIBLE);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (dropdown.getSelectedItemPosition() > 0) {
                    showState("project");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        projects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (projects.getSelectedItemPosition() > 0) {
                    showState("startDate");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        if (setDateFromCalendar.length() > 0 && isDateAndTimeSetFromCalendar) {
            String[] parts = setDateFromCalendar.split("-");
            mYear = Integer.parseInt(parts[0]);
            mMonth = Integer.parseInt(parts[1]);
            mDay = Integer.parseInt(parts[2]);
        }

        datePicker.init(mYear, mMonth, mDay, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
                showState("dateSelected");
            }
        });

        date_picker2.init(mYear, mMonth, mDay, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
                String date = getSelectedStartDateISONotation();
                String dateEnd = getSelectedEndDateISONotation();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date startDate = dateFormat.parse(date);
                    Date endDate = dateFormat.parse(dateEnd);
                    long different = endDate.getTime() - startDate.getTime();
                    if (different < 0) {
                        AlertDialog ad = new AlertDialog.Builder(getActivity())
                                .create();
                        ad.setCancelable(false);
                        ad.setTitle("Fehler");
                        ad.setMessage("Enddatum muss nach Startdatum sein!");
                        ad.setButton(Dialog.BUTTON_POSITIVE ,getActivity().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        ad.show();
                    } else {
                        showState("submitWithEndDate");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        timeSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showState("durationHour");
            }
        });

        durationHour.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (durationHour.getSelectedItemPosition() > 0) {
                    showState("durationMinute");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        durationMinutes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (durationMinutes.getSelectedItemPosition() > 0) {
                    float duration = (float)(durationHour.getSelectedItemPosition() - 1) + ((float)(durationMinutes.getSelectedItemPosition() - 1)  / 60f);
                    if (duration == 0) {
                        AlertDialog ad = new AlertDialog.Builder(getActivity())
                                .create();
                        ad.setCancelable(false);
                        ad.setTitle("Fehler");
                        ad.setMessage("Dauer darf nicht 0 Stunden und 0 Minuten sein!");
                        ad.setButton(Dialog.BUTTON_POSITIVE ,getActivity().getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        ad.show();
                    } else {
                        showState("submit");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        errorResultView.setText("Eintrag erfolgreich");
                        clickedStepResultText.setText("");
                        errorResultView.setVisibility(View.INVISIBLE);
                        progressBar.setVisibility(View.VISIBLE);
                    }
                });

                int eventType = dropdown.getSelectedItemPosition();
                String date = getSelectedStartDateISONotation();

                String formattedHour = String.format("%02d", timePicker.getCurrentHour());
                String formattedMinute = String.format("%02d", timePicker.getCurrentMinute());
                String time = date + " " + formattedHour + ":" + formattedMinute + ":00";
                String comment = commentView.getText().toString();
                String text = projects.getSelectedItem().toString();
                int projectId = 0;
                if (text.length() > 0) {
                    String[] parts = text.split(" - ");
                    projectId = Integer.parseInt(parts[0]);
                }

                float duration = 0;

                if (checkBox.isChecked() == true) {
                    int endDay = date_picker2.getDayOfMonth();

                    int endday = date_picker2.getDayOfMonth();
                    String formattedendDay = String.format("%02d", endday);
                    int endmonth = date_picker2.getMonth() + 1;
                    String formattedendMonth = String.format("%02d", endmonth);
                    int endyear = date_picker2.getYear();
                    String dateEnd = endyear +"-" + formattedendMonth +"-" + formattedendDay;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        Date startDate = dateFormat.parse(date);
                        Date endDate = dateFormat.parse(dateEnd);
                        long different = endDate.getTime() - startDate.getTime();
                        if (different < 0) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    errorResultView.setText("Enddatum muss nach Startdatum sein!");
                                    errorResultView.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            });

                            return;
                        }

                        time = date + " 00:00:00";
                        if (different == 0) {
                            duration = 24;
                        } else {
                            long secondsInMilli = 1000;
                            long minutesInMilli = secondsInMilli * 60;
                            long hoursInMilli = minutesInMilli * 60;
                            long daysInMilli = hoursInMilli * 24;

                            long elapsedDays = (different / daysInMilli) + 1;
                            duration = elapsedDays * 24;
                        }
                    } catch (Exception ex) {

                    }
                } else {
                    duration = (float)(durationHour.getSelectedItemPosition() - 1) + ((float)(durationMinutes.getSelectedItemPosition() - 1)  / 60f);
                    if (duration == 0) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                errorResultView.setText("Dauer darf nicht 0 Stunden und 0 Minuten sein");
                                errorResultView.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        });
                        return;
                    }
                }

                final String addTime = time;
                try {
                    SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
                    Cursor resultTimeKeepingsData = database.rawQuery("Select time from timekeepings WHERE time='" + addTime + "' and event_id = "+eventType, null);
                    if (resultTimeKeepingsData != null && resultTimeKeepingsData.getCount() > 0) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                errorResultView.setText("Eintrag zu dieser Zeit bereits vorhanden!");
                                errorResultView.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        });
                    } else {
                        PostTimeKeepData postTimeKeepData = new PostTimeKeepData(new PostTimeKeepData.PostTimeKeepDataListener() {
                            @Override
                            public void onFinished(String result) {
                                try {
                                    JSONArray response = new JSONArray(result);
                                    JSONObject innerObject = response.getJSONObject(0);
                                    if (innerObject.getString("status").equals("ok")) {
                                        SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
                                        database.execSQL("update timekeepings set synchronized = 1 WHERE time = '" + addTime + "'");
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                errorResultView.setText("Eintrag erfolgreich");
                                                clickedStepResultText.setText("");
                                                errorResultView.setVisibility(View.VISIBLE);
                                                progressBar.setVisibility(View.INVISIBLE);
                                                Calendar c = Calendar.getInstance();
                                                int year = c.get(Calendar.YEAR);
                                                int month = c.get(Calendar.MONTH);
                                                int day = c.get(Calendar.DAY_OF_MONTH);
                                                int hour = c.get(Calendar.HOUR_OF_DAY);
                                                int minute = c.get(Calendar.MINUTE);
                                                datePicker.init(year, month, day, null);
                                                timePicker.setCurrentHour(hour);
                                                timePicker.setCurrentMinute(minute);
                                                projects.setSelection(0);
                                                dropdown.setSelection(0);
                                                commentView.setText("");
                                                commentView.setVisibility(View.INVISIBLE);
                                            }
                                        });
                                    } else {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                errorResultView.setText("Eintrag fehlerhaft - bitte wiederholen!");
                                                errorResultView.setVisibility(View.VISIBLE);
                                                progressBar.setVisibility(View.INVISIBLE);
                                            }
                                        });
                                    }
                                } catch (Exception ex) {
                                    Log.d("Erorr", ex.getMessage());
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            errorResultView.setText("Eintrag fehlerhaft - bitte wiederholen!");
                                            errorResultView.setVisibility(View.VISIBLE);
                                            progressBar.setVisibility(View.INVISIBLE);
                                        }
                                    });
                                }

                                new Timer().schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                errorResultView.setText("");
                                                errorResultView.setVisibility(View.INVISIBLE);
                                            }
                                        });
                                    }
                                }, 5000);
                            }
                        }, token, eventType, comment, time, duration, projectId, true);
                        postTimeKeepData.execute();
                    }
                } catch (Exception e) {
                    Log.d("Exception", e.getMessage());
                }
            }
        });

        if (isDateAndTimeSetFromCalendar) {
            dropdown.setVisibility(View.INVISIBLE);
            datePicker.setVisibility(View.INVISIBLE);
            projects.setVisibility(View.INVISIBLE);
            date_picker2.setVisibility(View.INVISIBLE);
            checkBox.setVisibility(View.INVISIBLE);
            durationHour.setVisibility(View.INVISIBLE);
            timePicker.setVisibility(View.INVISIBLE);
            timeSubmitButton.setVisibility(View.INVISIBLE);
            commentView.setVisibility(View.INVISIBLE);
            submitButton.setVisibility(View.INVISIBLE);
            errorResultView.setVisibility(View.INVISIBLE);
            showState("start");
        }

        return root;
    }

    public void setDateAndTimeDataFromCalendar(String date, int time) {
        this.isDateAndTimeSetFromCalendar = true;
        setTimeFromCalendar = time;
        setDateFromCalendar = date;
        if (dropdown != null) {
            dropdown.setVisibility(View.INVISIBLE);
            datePicker.setVisibility(View.INVISIBLE);
            projects.setVisibility(View.INVISIBLE);
            date_picker2.setVisibility(View.INVISIBLE);
            checkBox.setVisibility(View.INVISIBLE);
            durationHour.setVisibility(View.INVISIBLE);
            timePicker.setVisibility(View.INVISIBLE);
            timeSubmitButton.setVisibility(View.INVISIBLE);
            commentView.setVisibility(View.INVISIBLE);
            submitButton.setVisibility(View.INVISIBLE);
            errorResultView.setVisibility(View.INVISIBLE);
            showState("start");
        }
    }

    public void showState(String state) {
        if (state.equals("startDate") && isDateAndTimeSetFromCalendar) {
            state = "dateSelected";
            isDateAndTimeSetFromCalendar = false;
            projects.setVisibility(View.INVISIBLE);
        }

        currentState = state;
        if (state.equals("start")) {
            projects.setVisibility(View.INVISIBLE);
            dropdown.setVisibility(View.VISIBLE);
            headerText.setText("Was soll eingetragen werden?");
            clickedStepResultText.setText("");
        } else if (state.equals("project")) {
            projects.setVisibility(View.VISIBLE);
            dropdown.setVisibility(View.INVISIBLE);
            datePicker.setVisibility(View.INVISIBLE);
            checkBox.setVisibility(View.INVISIBLE);
            headerText.setText("Projekt wählen.");
            currentState = "project";
            clickedStepResultText.setText(timekeep_array[dropdown.getSelectedItemPosition()]);
        } else if (state.equals("startDate")) {
            headerText.setText("Start-Datum wählen.");
            checkBox.setVisibility(View.VISIBLE);
            datePicker.setVisibility(View.VISIBLE);
            projects.setVisibility(View.INVISIBLE);
            date_picker2.setVisibility(View.INVISIBLE);
            durationHour.setVisibility(View.INVISIBLE);
            timePicker.setVisibility(View.INVISIBLE);
            timeSubmitButton.setVisibility(View.INVISIBLE);
            clickedStepResultText.setText(timekeep_array[dropdown.getSelectedItemPosition()] +"\nbei " + (String)projects.getAdapter().getItem(projects.getSelectedItemPosition()));
        } else if (state.equals("dateSelected")) {
            datePicker.setVisibility(View.INVISIBLE);
            checkBox.setVisibility(View.INVISIBLE);
            if (checkBox.isChecked()) {
                date_picker2.setVisibility(View.VISIBLE);
                headerText.setText("Ende-Datum wählen.");
                currentState = "endDate";
            } else {
                timePicker.setVisibility(View.VISIBLE);
                timeSubmitButton.setVisibility(View.VISIBLE);
                headerText.setText("Start-Zeit wählen.");
                currentState = "startTime";
            }

            clickedStepResultText.setText(timekeep_array[dropdown.getSelectedItemPosition()] +"\nbei " + (String)projects.getAdapter().getItem(projects.getSelectedItemPosition())
                    + "\nvom "
                    + getSelectedStartDate());
        } else if (state.equals("endDate")) {
            commentView.setVisibility(View.INVISIBLE);
            submitButton.setVisibility(View.INVISIBLE);
            errorResultView.setVisibility(View.INVISIBLE);
            datePicker.setVisibility(View.INVISIBLE);
            checkBox.setVisibility(View.INVISIBLE);
            date_picker2.setVisibility(View.VISIBLE);
            headerText.setText("Ende-Datum wählen.");
            clickedStepResultText.setText(timekeep_array[dropdown.getSelectedItemPosition()] +"\nbei " + (String)projects.getAdapter().getItem(projects.getSelectedItemPosition())
                    + "\nvom "
                    + getSelectedStartDate());
        } else if (state.equals("submitWithEndDate")) {
            date_picker2.setVisibility(View.INVISIBLE);
            commentView.setVisibility(View.VISIBLE);
            submitButton.setVisibility(View.VISIBLE);
            errorResultView.setVisibility(View.VISIBLE);
            headerText.setText("Kommentar vergeben und eintragen.");

            int day = date_picker2.getDayOfMonth();
            String formattedDay = String.format("%02d", day);
            int month = date_picker2.getMonth() + 1;
            String formattedMonth = String.format("%02d", month);
            int year = date_picker2.getYear();
            String date = formattedDay + "." + formattedMonth + "." + year;

            clickedStepResultText.setText(timekeep_array[dropdown.getSelectedItemPosition()] +"\nbei " + (String)projects.getAdapter().getItem(projects.getSelectedItemPosition())
                    + "\nvom "
                    + getSelectedStartDate()
                    + "\nbis zum "
                    + date);
        } else if (state.equals("durationHour")) {
            timeSubmitButton.setVisibility(View.INVISIBLE);
            timePicker.setVisibility(View.INVISIBLE);
            durationHour.setVisibility(View.VISIBLE);
            durationMinutes.setVisibility(View.INVISIBLE);
            headerText.setText("Dauer in Stunden angeben");
            clickedStepResultText.setText(timekeep_array[dropdown.getSelectedItemPosition()] +"\nbei " + (String)projects.getAdapter().getItem(projects.getSelectedItemPosition())
                    + "\nvom "
                    + getSelectedStartDate()
                    + ", "
                    + getSelectedTime());
        } else if (state.equals("durationMinute")) {
            durationHour.setVisibility(View.INVISIBLE);
            durationMinutes.setVisibility(View.VISIBLE);
            commentView.setVisibility(View.INVISIBLE);
            submitButton.setVisibility(View.INVISIBLE);
            errorResultView.setVisibility(View.INVISIBLE);
            headerText.setText("Dauer Minuten angeben");
            clickedStepResultText.setText(timekeep_array[dropdown.getSelectedItemPosition()] +"\nbei " + (String)projects.getAdapter().getItem(projects.getSelectedItemPosition())
                    + "\nvom "
                    + getSelectedStartDate()
                    + ", "
                    + getSelectedTime()
                    + "\nDauer: "
                    + (durationHour.getSelectedItemPosition() - 1) + "h");
        } else if (state.equals("submit")) {
            durationMinutes.setVisibility(View.INVISIBLE);
            commentView.setVisibility(View.VISIBLE);
            submitButton.setVisibility(View.VISIBLE);
            errorResultView.setVisibility(View.VISIBLE);
            headerText.setText("Kommentar vergeben und eintragen.");
            currentState = "submit";
            clickedStepResultText.setText(timekeep_array[dropdown.getSelectedItemPosition()] +"\nbei " + (String)projects.getAdapter().getItem(projects.getSelectedItemPosition())
                    + "\nvom "
                    + getSelectedStartDate()
                    + ", "
                    + getSelectedTime()
                    + "\nDauer: "
                    + (durationHour.getSelectedItemPosition() - 1) + "h"
                    + " "
                    + (durationMinutes.getSelectedItemPosition() - 1) + "min");
        }
    }

    private String getSelectedStartDate() {
        int day = datePicker.getDayOfMonth();
        String formattedDay = String.format("%02d", day);
        int month = datePicker.getMonth() + 1;
        String formattedMonth = String.format("%02d", month);
        int year = datePicker.getYear();
        String date = formattedDay + "." + formattedMonth +"." +  year;
        return date;
    }
    private String getSelectedEndDateISONotation() {
        int endDay = date_picker2.getDayOfMonth();
        int endday = date_picker2.getDayOfMonth();
        String formattedendDay = String.format("%02d", endday);
        int endmonth = date_picker2.getMonth() + 1;
        String formattedendMonth = String.format("%02d", endmonth);
        int endyear = date_picker2.getYear();
        String dateEnd = endyear +"-" + formattedendMonth +"-" + formattedendDay;
        return dateEnd;
    }

    private String getSelectedStartDateISONotation() {
        int day = datePicker.getDayOfMonth();
        String formattedDay = String.format("%02d", day);
        int month = datePicker.getMonth() + 1;
        String formattedMonth = String.format("%02d", month);
        int year = datePicker.getYear();
        String date = year +"-" + formattedMonth +"-" + formattedDay;
        return date;
    }

    private String getSelectedTime() {
        String formattedHour = String.format("%02d", timePicker.getCurrentHour());
        String formattedMinute = String.format("%02d", timePicker.getCurrentMinute());
        String time = formattedHour + ":" + formattedMinute + " Uhr";
        return time;
    }

    public boolean onBackPressed() {
        if (currentState.isEmpty() || currentState.equals("start")) {
            return true;
        }

        if (currentState.equals("project")) {
            showState("start");
        } else if (currentState.equals("startDate")) {
            showState("project");
        } else if (currentState.equals("endDate")) {
            showState("startDate");
        } else if (currentState.equals("startTime")) {
            showState("startDate");
        } else if (currentState.equals("submitWithEndDate")) {
            showState("endDate");
        } else if (currentState.equals("durationHour")) {
            showState("startTime");
        } else if (currentState.equals("durationMinute")) {
            showState("durationHour");
        } else if (currentState.equals("submit")) {
            showState("durationMinute");
        }

        return false;
    }

    public void addItems(String text, boolean atBeginning) {
        if (adapter != null) {
            boolean add = true;
            for (int i= 0; i < adapter.getCount(); i++) {
                if (adapter.getItem(i).contentEquals(text)) {
                    add = false;
                }
            }

            if (add) {
                if (atBeginning == true) {
                    adapter.insert(text, 0);
                } else {
                    adapter.add(text);
                }
            }
        } else {
            itemstoAdd.add(text);
        }
    }

    public void setToken(String token) {
        this.token = token;
    }

}
