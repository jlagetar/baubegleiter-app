package com.lagetar.baubegleiter.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lagetar.baubegleiter.BaubegleiterApplication;
import com.lagetar.baubegleiter.R;
import com.lagetar.baubegleiter.Start;
import com.lagetar.baubegleiter.domain.Clique;
import com.lagetar.baubegleiter.domain.Cluster;
import com.lagetar.baubegleiter.domain.Event;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CalenderFragment extends Fragment {
    private ArrayAdapter<String> adapter = null;
    private String curDate = null;
    ArrayList<String> listItems=new ArrayList<String>();
    private CalendarDay currentSelectedDate = null;

    private final int MINUTES_IN_A_HOUR = 24 * 60;
    private List<Event> events = new ArrayList<>();

    private RelativeLayout rlCalendarRoot;

    public static Fragment newInstance(Context context) {
        CalenderFragment f = new CalenderFragment();
        return f;
    }

    public void drawEvents(List<Event> events) {
        if (events != null && !events.isEmpty()) {
            Collections.sort(events, new TimeComparator());
            int screenWidth = rlCalendarRoot.getWidth();
            int screenHeight = (int)((23f * getResources().getDimension(R.dimen.hour_divider_height) + (24f * getResources().getDimension(R.dimen.hour_divider_margin_top))));

            List<Cluster> clusters = createClusters(createCliques(events));

            int id = 1;
            for (Cluster c : clusters) {
                for (Event event : c.getEvents()) {
                    int eventWidth = screenWidth / c.getMaxCliqueSize();
                    int leftMargin = c.getNextPosition() * eventWidth;
                    int eventHeight = minutesToPixels(screenHeight, event.getEndTimeInMinutes() + 1) - minutesToPixels(screenHeight, event.getStartTimeInMinutes());
                    int topMargin = minutesToPixels(screenHeight, event.getStartTimeInMinutes());

                    TextView eventView = new TextView(getActivity().getApplicationContext());
                    final Event data = event;
                    eventView.setText(getString(R.string.event_name_format, event.getName()));
                    eventView.setTextColor(Color.BLACK);
                    eventView.setBackgroundResource(R.drawable.event_bg);
                    eventView.setId(id);
                    eventView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which){
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    Start startActivity = (Start)getActivity();
                                                    startActivity.setEventToChange(data);
                                                    startActivity.changeFragment(getActivity().getString(R.string.menu_timekeep));
                                                    break;

                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    break;
                                            }
                                        }
                                    };

                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage("Wollen Sie diesen Eintrag wirklich bearbeiten?").setPositiveButton("Ja", dialogClickListener)
                                            .setNegativeButton("Nein", dialogClickListener).show();
                                }
                            });


                        }
                    });
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(eventWidth, eventHeight);
                    params.setMargins(leftMargin + 100, topMargin, 0, 0);

                    rlCalendarRoot.addView(eventView, params);
                    id++;
                }
            }
        }
    }

    private int minutesToPixels(int screenHeight, int minutes){
        float ht_px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 400, getResources().getDisplayMetrics());
        return (screenHeight * minutes) / MINUTES_IN_A_HOUR + (int)ht_px;
    }

    public static List<Clique> createCliques(List<Event> events) {
        int startTime = events.get(0).getStartTimeInMinutes();
        int endTime = events.get(events.size() - 1).getEndTimeInMinutes();

        List<Clique> cliques = new ArrayList<>();

        for(int i = startTime; i <= endTime; i++){
            Clique c = null;
            for(Event e : events){
                if(e.getStartTimeInMinutes() <= i && e.getEndTimeInMinutes() >= i){
                    if(c == null){
                        c = new Clique();
                    }
                    c.addEvent(e);
                }
            }
            if(c != null){
                if(!cliques.contains(c)) {
                    cliques.add(c);
                }
            }
        }
        return cliques;
    }

    public static List<Cluster> createClusters(List<Clique> cliques) {
        List<Cluster> clusters = new ArrayList<>();
        Cluster cluster = null;
        for(Clique c : cliques){
            if(cluster == null){
                cluster = new Cluster();
                cluster.addClique(c);
            } else {
                if (cluster.getLastClique().intersects(c)) {
                    cluster.addClique(c);
                } else {
                    clusters.add(cluster);
                    cluster = new Cluster();
                    cluster.addClique(c);
                }
            }
        }
        if(cluster != null){
            clusters.add(cluster);
        }
        return clusters;
    }

    public static class TimeComparator implements Comparator {
        public int compare(Object obj1, Object obj2) {
            Event o1 = (Event)obj1;
            Event o2 = (Event)obj2;
            float change1 = o1.getStartTime();
            float change2 = o2.getStartTime();
            if (change1 < change2) {
                return -1;
            }
            if (change1 > change2) {
                return 1;
            }
            float change3 = o1.getEndTime();
            float change4 = o2.getEndTime();
            if (change3 < change4) return -1;
            if (change3 > change4) return 1;
            return 0;
        }
    }

    private void AddClickListenerToTextView(TextView view, final int time) {
        //view.setKeyListener(null);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Start)getActivity()).startTimeKeepFragment(currentSelectedDate, time);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.calender, null);
        rlCalendarRoot = (RelativeLayout)root.findViewById(R.id.rl_calendar_root);
        final MaterialCalendarView calendarView = (MaterialCalendarView ) root.findViewById(R.id.calendarView);

        final TextView text0 = root.findViewById(R.id.text0);
        AddClickListenerToTextView(text0, 0);
        final TextView text1 = root.findViewById(R.id.text1);
        AddClickListenerToTextView(text1, 1);
        final TextView text2 = root.findViewById(R.id.text2);
        AddClickListenerToTextView(text2, 2);
        final TextView text3 = root.findViewById(R.id.text3);
        AddClickListenerToTextView(text3, 3);
        final TextView text4 = root.findViewById(R.id.text4);
        AddClickListenerToTextView(text4, 4);
        final TextView text5 = root.findViewById(R.id.text5);
        AddClickListenerToTextView(text5, 5);
        final TextView text6 = root.findViewById(R.id.text6);
        AddClickListenerToTextView(text6, 6);
        final TextView text7 = root.findViewById(R.id.text7);
        AddClickListenerToTextView(text7, 7);
        final TextView text8 = root.findViewById(R.id.text8);
        AddClickListenerToTextView(text8, 8);
        final TextView text9 = root.findViewById(R.id.text9);
        AddClickListenerToTextView(text9, 9);
        final TextView text10 = root.findViewById(R.id.text10);
        AddClickListenerToTextView(text10, 10);
        final TextView text11 = root.findViewById(R.id.text11);
        AddClickListenerToTextView(text11, 11);
        final TextView text12 = root.findViewById(R.id.text12);
        AddClickListenerToTextView(text12, 12);
        final TextView text13 = root.findViewById(R.id.text13);
        AddClickListenerToTextView(text13, 13);
        final TextView text14 = root.findViewById(R.id.text14);
        AddClickListenerToTextView(text14, 14);
        final TextView text15 = root.findViewById(R.id.text15);
        AddClickListenerToTextView(text15, 15);
        final TextView text16 = root.findViewById(R.id.text16);
        AddClickListenerToTextView(text16, 16);
        final TextView text17 = root.findViewById(R.id.text17);
        AddClickListenerToTextView(text17, 17);
        final TextView text18 = root.findViewById(R.id.text18);
        AddClickListenerToTextView(text18, 18);
        final TextView text19 = root.findViewById(R.id.text19);
        AddClickListenerToTextView(text19, 19);
        final TextView text20 = root.findViewById(R.id.text20);
        AddClickListenerToTextView(text20, 20);
        final TextView text21 = root.findViewById(R.id.text21);
        AddClickListenerToTextView(text21, 21);
        final TextView text22 = root.findViewById(R.id.text22);
        AddClickListenerToTextView(text22, 22);
        final TextView text23 = root.findViewById(R.id.text23);
        AddClickListenerToTextView(text23, 23);
        final TextView text24 = root.findViewById(R.id.text24);
        AddClickListenerToTextView(text24, 24);

        ListView dayListView  = (ListView) root.findViewById(R.id.dayListView);
        adapter =   new ArrayAdapter<String>(getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_1,
                listItems);

        dayListView.setAdapter(adapter);
        calendarView.addDecorators(new DayViewDecorator() {
            @Override
            public boolean shouldDecorate(CalendarDay day) {
                try {
                    int dayVal = day.getDay();
                    int month = day.getMonth();
                    int year = day.getYear();
                    String strMonth = String.valueOf(month);
                    String strMonthPadded = strMonth;
                    if (month + 1 < 10) {
                        strMonthPadded = "0" + strMonthPadded;
                    }
                    String strDay = String.valueOf(dayVal);
                    String strDayPadded = strDay;
                    if (dayVal < 10) {
                        strDayPadded = "0" + strDayPadded;
                    }

                    curDate = String.valueOf(year) + "-" + strMonthPadded + "-" + strDayPadded;
                    SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
                    Cursor results = database.rawQuery("Select * from timekeepings WHERE time like '" + curDate + "%'", null);
                    if (results != null && results.moveToFirst()) {
                        return true;
                    }
                } catch (Exception ex) {
                    Log.d("Fehler bei Datumspunkt", ex.getMessage());
                }

                return false;
            }

            @Override
            public void decorate(DayViewFacade view) {
                view.addSpan(new DotSpan(8, getResources().getColor(R.color.colorPrimary)));
            }
        });

        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                currentSelectedDate = date;
                int day = date.getDay();
                int month = date.getMonth();
                int year = date.getYear();
                String strMonth = String.valueOf(month);
                String strMonthPadded = strMonth;
                if (month + 1 < 10) {
                    strMonthPadded = "0" + strMonthPadded;
                }
                String strDay = String.valueOf(day);
                String strDayPadded = strDay;
                if (day < 10) {
                    strDayPadded = "0" + strDayPadded;
                }
                curDate = String.valueOf(year) + "-" + strMonthPadded + "-" + strDayPadded;
                updateDayList();
            }
        });

        dayListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    String str = (String)parent.getItemAtPosition(position);
                    String[] parts = str.split(", ");
                    String category = parts[0].replace("Ereignis: ", "");
                    String[] categories = getResources().getStringArray(R.array.timekeep_array);
                    String time = parts[parts.length - 1].replace("Zeitpunkt: ", "");
                    int category_id = -1;
                    for (int i=0; i < categories.length; i++) {
                        if (categories[i].equals(category)) {
                            category_id = i + 1;
                            break;
                        }
                    }

                    final int categoryId = category_id;
                    final String timeString = time;

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which){
                                        case DialogInterface.BUTTON_POSITIVE:
                                            try {
                                                SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
                                                database.execSQL("Delete FROM timekeepings WHERE time like '"+timeString+"%' and event_id="+categoryId);
                                                updateDayList();
                                            } catch (Exception ex) {
                                                Log.d("Löschen fehlerhaft", ex.getMessage());
                                            }
                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            break;
                                    }
                                }
                            };

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage("Wollen Sie diesen Eintrag wirklich löschen?").setPositiveButton("Ja", dialogClickListener)
                                    .setNegativeButton("Nein", dialogClickListener).show();
                        }
                    });
                } catch (Exception ex) {
                    Log.d("Klick fehlerhaft", ex.getMessage());
                }
            }
        });

        currentSelectedDate = calendarView.getSelectedDate();
        return root;
    }

    private void updateDayList() {
        adapter.clear();
        for (int i=1; i <= events.size(); i++) {
            rlCalendarRoot.removeView(this.getView().getRootView().findViewById(i));
        }

        events.clear();

        try {
            SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
            Cursor results = database.rawQuery("Select * from timekeepings WHERE time like '" + curDate + "%'", null);
            int id = 1;
            if (results != null && results.moveToFirst()) {
                do {
                    String[] categories;
                    categories = getResources().getStringArray(R.array.timekeep_array);
                    int index = results.getInt(results.getColumnIndex("event_id")) - 1;
                    String comment = results.getString(results.getColumnIndex("comment"));
                    String time = results.getString(results.getColumnIndex("time")).replace(curDate+" ", "");
                    time = time.substring(0, 5);
                    float hour = Float.parseFloat(time.substring(0,2));
                    String min = time.substring(3);
                    float minutes = Float.parseFloat(min) / 60;
                    float totalTime = hour + minutes;

                    float duration = Float.parseFloat(results.getString(results.getColumnIndex("duration")));
                    float end = totalTime + duration;
                    if (end > 24f) {
                        end = 24f;
                    }

                    //adapter.add(categories[index] + ", "+  comment + ", " + time + " -- "+duration+"h");
                    events.add(new Event(id, time +": " + categories[index] + ", "+  comment, totalTime, end));
                    id++;
                } while (results.moveToNext());

                rlCalendarRoot.post(new Runnable() {
                    @Override
                    public void run() {
                        drawEvents(events);
                    }
                });
            }
        } catch (Exception e) {
            Log.d("Error", e.getMessage());
        }
    }
}
