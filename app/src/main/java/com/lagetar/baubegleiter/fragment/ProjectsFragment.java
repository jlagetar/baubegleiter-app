package com.lagetar.baubegleiter.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.lagetar.baubegleiter.BaubegleiterApplication;
import com.lagetar.baubegleiter.GridViewActivity;
import com.lagetar.baubegleiter.R;

import java.util.ArrayList;
import java.util.List;

public class ProjectsFragment extends Fragment {
    public static String PROJECTS_MODE_VIEW = "view";
    private ListView list = null;
    private ArrayAdapter<String> adapter = null;
    private String mode = null;
    ArrayList<String> listItems=new ArrayList<String>();
    private String token;
    private List<String> itemstoAdd = new ArrayList<String>();

    public static Fragment newInstance(Context context) {
        ProjectsFragment f = new ProjectsFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.projects, null);
        list = (ListView) root.findViewById(R.id.project_list);
        adapter =   new ArrayAdapter<String>(getActivity().getApplicationContext(),
                        android.R.layout.simple_list_item_1,
                        listItems);
        if (itemstoAdd.size() > 0) {
            for (int i=0; i < itemstoAdd.size(); i++) {
                addItems(itemstoAdd.get(i));
            }
        }

        list.setAdapter(adapter);
        list.setClickable(true);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                try {
                    Object o = list.getItemAtPosition(position);
                    String project = (String) o;
                    String[] projectidParts = project.split(" - ");
                    String projectNumber = projectidParts[0];
                    Intent intent = new Intent(getActivity(), GridViewActivity.class);
                    intent.putExtra("token", token);
                    Cursor resultProjectsData = BaubegleiterApplication.getInstance().getDatabase().rawQuery("Select * from projects WHERE number=" + projectNumber, null);
                    if (resultProjectsData != null && resultProjectsData.getCount() > 0) {
                        resultProjectsData.moveToFirst();
                        int projectId = resultProjectsData.getInt(resultProjectsData.getColumnIndex("project_id"));
                        intent.putExtra("project", String.valueOf(projectId));
                        startActivity(intent);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        return root;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void addItems(String text) {
        if (adapter != null) {
            boolean add = true;
            for (int i= 0; i < adapter.getCount(); i++) {
                if (adapter.getItem(i).contentEquals(text)) {
                    add = false;
                }
            }

            if (add) {
                adapter.add(text);
            }
        } else {
            itemstoAdd.add(text);
        }
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

}
