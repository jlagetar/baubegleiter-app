package com.lagetar.baubegleiter.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.lagetar.baubegleiter.BaubegleiterApplication;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;

public class Utils {

	private Context _context;
	private String _folder;

	// constructor
	public Utils(Context context, String folder) {
		this._context = context;
		this._folder = folder;
	}

	/*
	 * Reading file paths from SDCard
	 */
	public ArrayList<String> getFilePaths() {
		ArrayList<String> filePaths = new ArrayList<String>();
        String uploadImagPath = BaubegleiterApplication.getInstance().getImagePath("assets", "upload.jpg");
        try {
            InputStream in = _context.getAssets().open("upload.jpg");
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = in.read(buf);
            while (n != -1) {
                out.write(buf, 0, n);
                n = in.read(buf);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();

            FileOutputStream fos = new FileOutputStream(uploadImagPath);
            fos.write(response);
            fos.close();
            File f = new File(uploadImagPath);
            if (!f.exists()) {
                Log.d("Error", "No upload file");
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

		filePaths.add(uploadImagPath);
        String[] files = BaubegleiterApplication.getInstance().getImagesInFolder(_folder);
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
            	//need to check if this image is within database if not, we need
				try {
					SQLiteDatabase database = BaubegleiterApplication.getInstance().getDatabase();
					Cursor resultProjectImagesData = database.rawQuery("Select * from project_images WHERE path='" + files[i]+"'", null);
					if (resultProjectImagesData != null && resultProjectImagesData.getCount() > 0) {
						resultProjectImagesData.moveToFirst();
						int index = resultProjectImagesData.getColumnIndex("id");
						if (index >= 0) {
							int projectId = resultProjectImagesData.getInt(index);
							if (projectId >= 0) {
								filePaths.add(BaubegleiterApplication.getInstance().getImagePath(_folder, files[i]));
							}
						}
					}
				} catch (Exception ex) {
					Log.d("Error", ex.getMessage());
				}
            }
        }

		/*

		File directory = new File(
				android.os.Environment.getExternalStorageDirectory()
						+ File.separator + AppConstant.PHOTO_ALBUM);

		// check for directory
		if (directory.isDirectory()) {
			// getting list of file paths
			File[] listFiles = directory.listFiles();

			// Check for count
			if (listFiles.length > 0) {

				// loop through all files
				for (int i = 0; i < listFiles.length; i++) {

					// get file path
					String filePath = listFiles[i].getAbsolutePath();

					// check for supported file extension
					if (IsSupportedFile(filePath)) {
						// Add image path to array list
						filePaths.add(filePath);
					}
				}
			} else {
				// image directory is empty
				Toast.makeText(
						_context,
						AppConstant.PHOTO_ALBUM
								+ " is empty. Please load some images in it !",
						Toast.LENGTH_LONG).show();
			}

		} else {
			AlertDialog.Builder alert = new AlertDialog.Builder(_context);
			alert.setTitle("Error!");
			alert.setMessage(AppConstant.PHOTO_ALBUM
					+ " directory path is not valid! Please set the image directory name AppConstant.java class");
			alert.setPositiveButton("OK", null);
			alert.show();
		}*/

		return filePaths;
	}

	/*
	 * Check supported file extensions
	 * 
	 * @returns boolean
	 */
	private boolean IsSupportedFile(String filePath) {
		String ext = filePath.substring((filePath.lastIndexOf(".") + 1),
				filePath.length());

		if (AppConstant.FILE_EXTN
				.contains(ext.toLowerCase(Locale.getDefault())))
			return true;
		else
			return false;

	}

	/*
	 * getting screen width
	 */
	public int getScreenWidth() {
		int columnWidth;
		WindowManager wm = (WindowManager) _context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();

		final Point point = new Point();
		try {
			display.getSize(point);
		} catch (NoSuchMethodError ignore) { // Older device
			point.x = display.getWidth();
			point.y = display.getHeight();
		}
		columnWidth = point.x;
		return columnWidth;
	}
}
