package com.lagetar.baubegleiter;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import java.io.IOException;

public abstract class BApplication extends Application {
    private static BApplication instance = null;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        instance = null;
    }

    public abstract String getImagePath(String folder, String imageName);

    public abstract String[] getImagesInFolder(String folder);

    public abstract String makeDirectory(String directoryName);

    /**
     * Getter for the database.
     *
     * <p>In this method the database connection should be created.
     *
     * @return the database connection.
     * @throws IOException if something goes wrong.
     */
    public abstract SQLiteDatabase getDatabase() throws IOException;

    /**
     * Closes the database.
     */
    public abstract void closeDatabase();

    /**
     * Set the current app version.
     */
    public abstract void setVersion(String version);

    /**
     * Get the current app version.
     */
    public abstract String getVersion();

    /**
     * @return the singleton instance.
     */
    public static BApplication getInstance() {
        return instance;
    }

    public static String getWebUrl() {
        return "https://www.bau-begleiter.com/login/";
    }
}
